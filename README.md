# trimmomatic Singularity container
### Bionformatics package trimmomatic<br>
A flexible read trimming tool for Illumina NGS data<br>
trimmomatic Version: 0.39<br>
[http://www.usadellab.org/cms/?page=trimmomatic]

Singularity container based on the recipe: Singularity.trimmomatic_v0.39

Package installation using Miniconda3-4.7.12<br>

Image singularity (V>=3.3) is automatically build and deployed (gitlab-ci) and pushed on the registry using the .gitlab-ci.yml <br>

### build:
`sudo singularity build trimmomatic_v0.39.sif Singularity.trimmomatic_v0.39`

### Get image help
`singularity run-help ./trimmomatic_v0.39.sif`

#### Default runscript: STAR
#### Usage:
  `trimmomatic_v0.39.sif --help`<br>
    or:<br>
  `singularity exec trimmomatic_v0.39.sif trimmomatic --help`<br>


### Get image (singularity version >=3.3) with ORAS:<br>
`singularity pull trimmomatic_v0.39.sif oras://registry.forgemia.inra.fr/gafl/singularity/trimmomatic/trimmomatic:latest`


